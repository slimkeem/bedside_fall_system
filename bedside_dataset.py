import torch
from torch.utils.data import Dataset

import pandas as pd
import numpy as np
from matplotlib import cm

import cv2 as cv
from PIL import Image

import transforms as T

from bedside_utilities import *
from helper import *

def read_images(root_dir,data_frame):
    all_images = []
    for recording in range(data_frame.get_value(len(data_frame)-1, 'Recording')): #for 28 recordings
        image_path = root_dir + 'FLIR_' + str((recording+1)) + '.txt'    
        image_frame = pd.read_csv(image_path, delimiter='\s+', header=None)
        
        all_images.append(image_frame[:-1]) #Add all frames ecept the final one cuz i dunno, last frame is maybe false
        
    df2 = pd.concat(all_images)
            
    return df2
        

def get_transform(train):
    transforms = []
    # converts the image, a PIL image, into a PyTorch Tensor
    transforms.append(T.ToTensor())
    if train:
        # during training, randomly flip the training images
        # and ground-truth for data augmentation
        transforms.append(T.RandomHorizontalFlip(0.5))
    return T.Compose(transforms)


class BedsideFallsDataset(Dataset):    
    """Bedside Falls dataset"""
    
    def __init__(self, csv_file, root_dir, transform=None, return_type = 'default'):
        """
        Args:
            csv_file (string): Path to the csv file with annotations.
            root_dir (string): Directory with all the images.
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        data_frame  = pd.read_csv(csv_file)
        images_data_frame = read_images(root_dir,data_frame)        
        
        #CATEGORICAL ENCODE
        data_frame["Location_code"] = pd.Categorical(data_frame["Location"], categories = ['Floor','Bed','Bedrail']).codes
        data_frame["Posture_code"] = pd.Categorical(data_frame["Posture"], categories = ['Standing','Sitting','Laying']).codes
        #NOMINAL/ONEHOT ENCODE
        data_frame = pd.get_dummies(data_frame)  
        
        self.images = np.asarray(images_data_frame).astype('float64')
        self.data = np.asarray(data_frame)
        self.transform = transform
        self.return_type = return_type
        self.heat_trail_set = []
        self.bg_idx = 0
                
    def __len__(self):
        length = len(self.data)
        return length
    
    def __getitem__(self, idx):
        if torch.is_tensor(idx):
            idx = idx.tolist()
            
        final_data = self.data[idx]
        image = self.images[idx].reshape(60,80,1) #HxWxC will be converted to CxHxW by toTensor()            
        
        # set global variables per recording
        if final_data[1] == 1:
            self.heat_trail_set = []
            self.bg_idx = idx
        
        # Set Background frame per recording
        possible_bg = image
        possible_bg = self.images[self.bg_idx].reshape(60,80,1)
                
        # Normalize image to -1 & +1
        normalized_image = normalize_minus_plus(image)
        bg = normalize_minus_plus(possible_bg)
                
        # Get Background model of current frame 
        model_tresh = 0.09
        bg_mod = normalized_image*model_tresh + bg*(1-model_tresh)
        bg_model = abs(bg_mod-normalized_image)
        # Heat disposal            
        cleaned_image = get_masky_image_minus_residue(normalized_image, self.heat_trail_set)
        cleaned_image = normalize_minus_plus(cleaned_image)
                    
        # Masks 
        person_mask = get_mask(cleaned_image,thresh=np.array(cleaned_image).mean()*0.5,
                               iterations=3).reshape(60,80,1)
        bed_mask = get_mask(bg,thresh=0.43,iterations=1).reshape(60,80)         

        ret,bg_mask =  cv.threshold(np.array(bg_model),np.array(bg_model).mean(),
                                    np.array(bg_model).max(),cv.THRESH_BINARY)
        diff=person_mask-bg_mask.reshape(60,80,1)
        ret3,diff_mask =  cv.threshold(np.array(diff),np.array(diff).max()-0.1,
                                       np.array(diff).max(),cv.THRESH_BINARY)
        diff_mask=diff_mask.reshape(60,80)
        
        # Prepare targets
        boxes = []
        labels = []
        masks = []
        iscrowd = []
        area = []
        pers_label = 1
        bed_label = 2
        
        # Get Bounding Boxes and append labels
        pers_box = get_bbox(diff_mask)
        bed_box = get_bbox(bed_mask)
        
        if final_data[8] != -1 or final_data[9] != -1:
            if pers_box is not None:
                boxes.append(pers_box)
                labels.append(pers_label)
                masks.append(diff_mask)
                iscrowd.append(0)
                area.append((pers_box[3] - pers_box[1]) * (pers_box[2] - pers_box[0]))
                
        if bed_box is not None:
            boxes.append(bed_box)
            labels.append(0)
            masks.append(bed_mask)
            iscrowd.append(0)
            area.append((bed_box[3] - bed_box[1]) * (bed_box[2] - bed_box[0]))
        
        # Prepare to return
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels = torch.as_tensor(labels, dtype=torch.int64)
        masks = torch.as_tensor(masks, dtype=torch.uint8)
        iscrowd = torch.as_tensor(iscrowd, dtype=torch.int64)
        image_id = torch.tensor([idx])
        area = torch.tensor(area, dtype=torch.float32)
        
        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["masks"] = masks
        target["image_id"] = image_id
        target["area"] = area
        target["iscrowd"] = iscrowd
        
        # Transform to PIL Images
        img = Image.fromarray(np.uint8(cm.coolwarm(cleaned_image.reshape(60,80))*255))
        img = img.convert('RGB')
                
        if self.transform is not None:
            img,target = self.transform(img,target)
#             img,target = self.transform(img_sample,target)
            
        if self.return_type == 'default':
            return img,target
        
        if self.return_type == 'predictor':
            masks = []
            if final_data[8] != -1 or final_data[9] != -1:
                if pers_box is not None:
                    masks.append(diff_mask.reshape(60,80))
                else:
                    masks.append(np.zeros([60,80]))
            else: 
                masks.append(np.zeros([60,80]))
            masks = torch.as_tensor(masks, dtype=torch.uint8)
            distance = torch.tensor(final_data[6], dtype=torch.int)
            location = torch.tensor(final_data[8], dtype=torch.int)
            posture = torch.tensor(final_data[9], dtype=torch.int)
            inputs = {}
            inputs["img"] = img
            inputs["mask"] = masks
            inputs["distance"] = distance
            ######
            inputs["norm"] = normalized_image.reshape(60,80)
            inputs["bgmod"] = bg_mod.reshape(60,80)
            inputs["bgmodel"] = bg_model.reshape(60,80)
            inputs["clean"] = cleaned_image.reshape(60,80)
            inputs["person_mask"] = person_mask.reshape(60,80)
            inputs["diff_mask"] = diff_mask.reshape(60,80)           
        
            labels = {}
            labels['location'] = location
            labels['posture'] = posture

            return inputs,labels
        

        