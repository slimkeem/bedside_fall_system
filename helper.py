from __future__ import print_function, division
import math
import sys
import time

import torch
import torch.nn as nn

import matplotlib.pyplot as plt
from matplotlib import cm

import utils as ut

import torchvision.models.detection.mask_rcnn
import numpy as np

from coco_eval import CocoEvaluator
from sklearn.metrics import confusion_matrix, classification_report
import warnings

def plot_color_image(image):
#     %matplotlib inline
#     color_map = plt.cm.get_cmap('gist_rainbow')
    color_map = plt.cm.get_cmap('bwr')
#     reversed_color_map = color_map.reversed()

    plt.figure(dpi=100);
    plt.imshow(image, cmap=color_map)
    plt.colorbar();
    plt.show()

def train_one_epoch_edited(model, optimizer, data_loader, device, epoch, print_freq):
    model.train()
    metric_logger = ut.MetricLogger(delimiter="  ")
    metric_logger.add_meter('lr', ut.SmoothedValue(window_size=1, fmt='{value:.6f}'))
    header = 'Epoch: [{}]'.format(epoch)

    lr_scheduler = None
    if epoch == 0:
        warmup_factor = 1. / 1000
        warmup_iters = min(1000, len(data_loader) - 1)

        lr_scheduler = ut.warmup_lr_scheduler(optimizer, warmup_iters, warmup_factor)

    for images, targets in metric_logger.log_every(data_loader, print_freq, header):
        images = images[None,:,:,:].float()
        images = list(image.to(device) for image in images)
        targets = [{k: v.to(device) for k, v in t.items()} for t in [targets]]

        loss_dict = model(images, targets)

        losses = sum(loss for loss in loss_dict.values())

        # reduce losses over all GPUs for logging purposes
        loss_dict_reduced = ut.reduce_dict(loss_dict)
        losses_reduced = sum(loss for loss in loss_dict_reduced.values())

        loss_value = losses_reduced.item()

        if not math.isfinite(loss_value):
            print("Loss is {}, stopping training".format(loss_value))
            print(loss_dict_reduced)
            sys.exit(1)

        optimizer.zero_grad()
        losses.backward()
        optimizer.step()

        if lr_scheduler is not None:
            lr_scheduler.step()

        metric_logger.update(loss=losses_reduced, **loss_dict_reduced)
        metric_logger.update(lr=optimizer.param_groups[0]["lr"])

        
def _get_iou_types(model):
    model_without_ddp = model
    if isinstance(model, torch.nn.parallel.DistributedDataParallel):
        model_without_ddp = model.module
    iou_types = ["bbox"]
    if isinstance(model_without_ddp, torchvision.models.detection.MaskRCNN):
        iou_types.append("segm")
    if isinstance(model_without_ddp, torchvision.models.detection.KeypointRCNN):
        iou_types.append("keypoints")
    return iou_types

@torch.no_grad()
def evaluate_edited(model, data_loader, device):
    n_threads = torch.get_num_threads()
    # FIXME remove this and make paste_masks_in_image run on the GPU
    torch.set_num_threads(1)
    cpu_device = torch.device("cpu")
    model.eval()
    metric_logger = ut.MetricLogger(delimiter="  ")
    header = 'Test:'

    coco = get_coco_api_from_dataset(data_loader.dataset)
    iou_types = _get_iou_types(model)
    coco_evaluator = CocoEvaluator(coco, iou_types)

    for image, targets in metric_logger.log_every(data_loader, 100, header):
        image = image[None,:,:,:].float()
        image = list(img.to(device) for img in image)
        targets = [{k: v.to(device) for k, v in t.items()} for t in [targets]]

        torch.cuda.synchronize()
        model_time = time.time()
        outputs = model(image)

        outputs = [{k: v.to(cpu_device) for k, v in t.items()} for t in outputs]
        model_time = time.time() - model_time

        res = {target["image_id"].item(): output for target, output in zip(targets, outputs)}
        evaluator_time = time.time()
        coco_evaluator.update(res)
        evaluator_time = time.time() - evaluator_time
        metric_logger.update(model_time=model_time, evaluator_time=evaluator_time)

    # gather the stats from all processes
    metric_logger.synchronize_between_processes()
    print("Averaged stats:", metric_logger)
    coco_evaluator.synchronize_between_processes()

    # accumulate predictions from all images
    coco_evaluator.accumulate()
    coco_evaluator.summarize()
    torch.set_num_threads(n_threads)
    return coco_evaluator

def train_locator(model, optimizer, whole_loader, device, epoch, print_freq, criterion, mask_model):
    location_model = None
    train_predictor(model, optimizer, whole_loader, device, epoch, print_freq, 
                    criterion, mask_model, location_model)

def train_poser(model, optimizer, whole_loader, device, epoch, print_freq, criterion, mask_model, location_model):
    train_predictor(model, optimizer, whole_loader, device, epoch, print_freq, 
                    criterion, mask_model, location_model)


def train_predictor(model, optimizer, whole_loader, device, epoch, print_freq, 
                    criterion, mask_model, location_model):
    running_loss = 0.0
    lr_scheduler = None
    if epoch == 0:
        warmup_factor = 1. / 1000
        warmup_iters = min(1000, len(whole_loader) - 1)

        lr_scheduler = ut.warmup_lr_scheduler(optimizer, warmup_iters, warmup_factor)
    for i, data in enumerate(whole_loader, 0):
        # get the inputs; data is a list of [inputs, labels]
        inputs, labels = data
        masks = get_pred_mask(inputs['img'], mask_model, device)
        
#         masks = masks.view(masks.size(0), -1)
        masks = (masks[0].float()).to(device)
        distance = inputs['distance'].reshape(1)
        distance = distance.view(distance.size(0),-1)
        distance = distance.float().to(device)
        #TRY CONCATING THE BOUNDING BOX ALSO
        
        # zero the parameter gradients
        optimizer.zero_grad()
        
        if (location_model is None):
            label = labels['location'].long()
            label = label.reshape(1)
            label = label.to(device)
            x = masks        
            outputs = model(x)
        elif (location_model is not None): 
            location = location_model(masks)
            location = location.view(location.size(0), -1)
            location = (location.float()).to(device)
            label = labels['posture'].long()
            label = label.reshape(1)
            label = label.to(device)
#             x = torch.cat((masks,distance,location), dim=1)
            x1 = masks
            x2 = distance
#             x2 = torch.cat((distance,location), dim=1)
            outputs = model(x1,x2)
        
        loss = criterion(outputs, label)
        loss.backward()
        optimizer.step()
        
        if lr_scheduler is not None:
            lr_scheduler.step()

        # print statistics
        running_loss += loss.item()
        if i % print_freq == (print_freq-1):    # print every print_freq mini-batches
            print('learning rate: ', optimizer.param_groups[0]["lr"])
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / print_freq))
            running_loss = 0.0
            
            
def eval_locator(model, whole_loader_test, classes, device, print_freq, mask_model):
    location_model = None
    evaluate_predictor(model, whole_loader_test, classes, 
                       device, print_freq, mask_model, location_model)

def eval_poser(model, whole_loader_test, classes, device, print_freq, mask_model, location_model):
    evaluate_predictor(model, whole_loader_test, classes, 
                       device, print_freq, mask_model, location_model)

def evaluate_predictor(model, whole_loader_test, classes, device, print_freq, mask_model, location_model):
    pred = []
    true = []
    pred_wrong = []
    true_wrong = []
    correct = 0
    total = 0
    wrong = 0
    class_correct = list(0. for i in range(len(classes)))
    class_total = list(0. for i in range(len(classes)))
    class_wrong = list(0. for i in range(len(classes)))
    with torch.no_grad():
        for i, data in enumerate(whole_loader_test,0): 
            inputs, labels = data
            
            masks = get_pred_mask(inputs['img'], mask_model, device)
#             masks = masks.view(masks.size(0), -1)
            masks = (masks[0].float()).to(device)
            distance = inputs['distance'].reshape(1)
            distance = distance.view(distance.size(0),-1)
            distance = distance.float().to(device)
            
            if (location_model is None):
                label = labels['location'].long()
                label = label.reshape(1)
                label = label.to(device)     #target
                x = masks                    #data
                outputs = model(x)
            elif (location_model is not None): 
                location = location_model(masks)
                location = location.view(location.size(0), -1)
                location = (location.float()).to(device)
                label = labels['posture'].long()
                label = label.reshape(1)
                label = label.to(device) 
#                 x = torch.cat((masks,distance,location), dim=1)
                x1 = masks
                x2 = torch.cat((distance,location), dim=1)
                outputs = model(x1,x2)
            
            _, predicted = torch.max(outputs.data, 1) 
            _, class_predicted = torch.max(outputs, 1) #preds
            
            c = (class_predicted == label).squeeze()
            not_c = (class_predicted != label).squeeze()
            
            total += label.size(0)
            correct += (predicted == label).sum().item()
            wrong += (predicted != label).sum().item()
            
            class_correct[label] += c.item() #running_corrects
            class_wrong[label] += not_c.item() #running_wrongs
            class_total[label] += 1
            
            preds = np.reshape(class_predicted.cpu(),(len(class_predicted),1))
            target = np.reshape(label.cpu(),(len(preds),1))
            for k in range(len(preds)):
                pred.append(preds[k])
                true.append(target[k])
                if(preds[k]!=target[k]):
                    pred_wrong.append(preds[k])
                    true_wrong.append(target[k])
            
        print('Accuracy of the network on the input data is: %d %%' % (
            100 * correct / total))
        for j in range(len(classes)):
            print('Accuracy of %5s : %2d %%' % (
                classes[j], 100 * class_correct[j] / class_total[j]))  
        plot_confusion_matrix(true, pred, classes=classes,title='Confusion matrix, without normalization')
        print(classification_report(true, pred))
                      
                    
def get_pred_mask(img,mask_model,device):
    """
        Going to use this to get mask to use in the 
    """
    img = img.float()
    mask_model.eval()
    with torch.no_grad():
        prediction = mask_model([img.to(device)])
        
    for preds in prediction:
        box = preds['boxes']
        score = preds['scores']
        mask = preds['masks']
        
        if len(box) == 0:
            return torch.zeros(1,1,1,60, 80)
            
        else:    
            area = (box[:,3]-box[:,1])*(box[:,2]-box[:,0])

            batch = (area, score, mask)
    #         print(batch)
    #         print ((t == 2).nonzero())
            index=(score==max(score)).nonzero()
            if area[index] <= 1500:
                right_one=mask[index]
            else: 
                if area[index] == min(area):
                    right_one=mask[index]
                else:
                    x = score
                    x = x[x!=x[index.item()]]
    #                 print(x)
    #                 new_index=(score==x.item()).nonzero()
                    new_index=(x==max(x)).nonzero()
    #                 print(new_index)
                    right_one=mask[new_index]
            return right_one
                

def plot_confusion_matrix(y_true, y_pred, classes,
                          normalize=False,
                          title=None,
                          cmap=plt.cm.Blues):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    if not title:
        if normalize:
            title = 'Normalized confusion matrix'
        else:
            title = 'Confusion matrix, without normalization'
    # Compute confusion matrix
#     cm = confusion_matrix(y_true, y_pred)
    cm = confusion_matrix(y_true, y_pred, labels=[0,1,2])
    # Only use the labels that appear in the data
    #classes = classes[unique_labels(y_true, y_pred)]
    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')
    fig, ax = plt.subplots()

    im = ax.imshow(cm, interpolation='nearest', cmap=cmap)
    ax.figure.colorbar(im, ax=ax)
    # We want to show all ticks...
    ax.set(xticks=np.arange(cm.shape[1]),
           yticks=np.arange(cm.shape[0]),
           # ... and label them with the respective list entries
           xticklabels=classes, yticklabels=classes,
           title=title,
           ylabel='True label',
           xlabel='Predicted label')
    # Rotate the tick labels and set their alignment.
    plt.setp(ax.get_xticklabels(), rotation=45, ha="right",
             rotation_mode="anchor")
    # Loop over data dimensions and create text annotations.
    fmt = '.2f' if normalize else 'd'
    thresh = cm.max() / 2.
    for i in range(cm.shape[0]):
        for j in range(cm.shape[1]):
            ax.text(j, i, format(cm[i, j], fmt),
                    ha="center", va="center",
                    color="white" if cm[i, j] > thresh else "black")
    fig.tight_layout()
    print(cm)
    return ax
