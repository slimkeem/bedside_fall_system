import numpy as np
import cv2 as cv

from helper import *

# Manually NORMALIZING image to between 1 and 0
# returns a torch tensor
def normalize_zero_one(image):
#     transformed_image = torch.as_tensor(image)
    min_v = np.min(image)
    range_v = np.max(image) - min_v
    if range_v > 0:
        norm_image = (image - min_v) / range_v
    else:
        norm_image = np.zeros(image.size())
    return norm_image

def normalize_minus_plus(image):
    #manually NOMRMALIZING between -1 and +1
    image = normalize_zero_one(image)
    mean = 0.5
    std = 0.5
    transformed_image = (image - mean)/std
    return transformed_image

# Heat disposal
def get_masky_image_minus_residue(image, heat_trail_set,frames=10, sensitivity=0.8):
    """
        Args:
            image: image to filter
            heat_trail_set: Empty set which would be used for filtering images in batches
            frames: Number of frames to use in filtering for residual heat
    """
    
    prev_heat_trail = (np.asarray(image)/frames*sensitivity)
    new_heat_trail = np.zeros([60,80])
    if len(heat_trail_set) != 0:
        test = prev_heat_trail - heat_trail_set[-1]
        if test.max() > 0.02 or test.min() < -0.02:
#             print("Probably some movement")
            heat_trail_set.append(prev_heat_trail)
            
        if len(heat_trail_set) == frames:
            new_heat_trail = sum(heat_trail_set) - heat_trail_set[0]
            del heat_trail_set[0] 
        new_heat_trail = sum(heat_trail_set)
    else:
        heat_trail_set.append(prev_heat_trail) 
    
    return image - new_heat_trail.reshape(60,80,1)

def get_mask(image, thresh=0.1, iterations=2):
    ret,mask = cv.threshold(np.array(image),thresh,1,cv.THRESH_BINARY)
    
    eliptical = np.array([[0, 0, 1, 0, 0],
                          [1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1],
                          [1, 1, 1, 1, 1],
                          [0, 0, 1, 0, 0]],np.uint8)

    opening = cv.morphologyEx(mask, cv.MORPH_OPEN, eliptical, iterations)
        
    return opening

def get_bbox(img):
    pos = np.where(img != 0)
    rows = np.any(img, axis=1)
    cols = np.any(img, axis=0)
    if rows.any() > 0 or cols.any()>0:
        rmin, rmax = np.where(rows)[0][[0, -1]]
        cmin, cmax = np.where(cols)[0][[0, -1]]
        return [cmin, rmin, cmax, rmax]
    else:
        cmin, rmin, cmax, rmax = 0,0,0,0
        return None
    
