import torch
import torch.nn as nn

from torchvision import transforms, utils, models
from torchvision.models.detection.faster_rcnn import FastRCNNPredictor
from torchvision.models.detection.mask_rcnn import MaskRCNNPredictor

from helper import *

def get_instance_segmentation_model(num_classes=3):
    # load an instance segmentation model pre-trained on COCO
    model = models.detection.maskrcnn_resnet50_fpn(pretrained=True)

    # get the number of input features for the classifier
    in_features = model.roi_heads.box_predictor.cls_score.in_features
    
    # replace the pre-trained head with a new one
    model.roi_heads.box_predictor = FastRCNNPredictor(in_features, num_classes)

    # now get the number of input features for the mask classifier
    in_features_mask = model.roi_heads.mask_predictor.conv5_mask.in_channels
    
    hidden_layer = 256
    
    # and replace the mask predictor with a new one
    model.roi_heads.mask_predictor = MaskRCNNPredictor(in_features_mask,
                                                       hidden_layer,
                                                       num_classes)

    return model

class Predictor(nn.Module):
    def __init__(self, D_in, H, D_out):
        super(Predictor, self).__init__()
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        self.fc1 = nn.Linear(D_in, H)
        self.fc2 = nn.Linear(H, D_out)
        
    def forward(self, inputs):
#         x = torch.cat((masks,distance), dim=1)
        h_relu = nn.functional.relu(self.fc1(inputs)) #for location
        y_pred = self.fc2(h_relu)
        
        return y_pred


class ConvPredictor(nn.Module):
    def __init__(self, D_in, D_out):
        super(ConvPredictor, self).__init__()
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        # 1 input image channel, 6 output channels, 3x3 square convolution
        # kernel
        self.D_in = D_in
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)
        # an affine operation: y = Wx + b
        self.fc1 = nn.Linear(3744, 120)  # 6*6 from image dimension
        self.fc2 = nn.Linear(120, 84)
        self.fc_only_mask = nn.Linear(84, D_out)
        self.fc3 = nn.Linear(88, 20) 
        self.fc4 = nn.Linear(20, 10)
        self.fc5 = nn.Linear(10, D_out)

    def forward(self, masks, extra_inputs=None):
#         x1 = self.cnn(image)
#         x2 = data
#         x = torch.cat((x1, x2), dim=1) 
        x1 = nn.functional.max_pool2d(nn.functional.relu(self.conv1(masks)), (2, 2))
        # If the size is a square you can only specify a single number
        x1 = nn.functional.max_pool2d(nn.functional.relu(self.conv2(x1)), 2)
#         x1 = x1.view(-1, self.num_flat_features(x1))
        x1 = x1.view(x1.size(0), -1)
        self.D_in=x1.shape
        x1 = nn.functional.relu(self.fc1(x1))
        x1 = nn.functional.relu(self.fc2(x1))
        y_pred = self.fc_only_mask(x1)
        if extra_inputs is not None:
            x2 = extra_inputs
            x = torch.cat((x1,x2), dim=1)
            h_relu = nn.functional.relu(self.fc3(x)) #for location
            h_relu = nn.functional.relu(self.fc4(h_relu))
            y_pred = self.fc5(h_relu)
        return y_pred

    def num_flat_features(self, inputs):
        size = inputs.size()[1:]  # all dimensions except the batch dimension
        num_features = 1
        for s in size:
            num_features *= s
        return num_features
    
class PosConvPredictor(nn.Module):
    def __init__(self, D_in, D_out):
        super(PosConvPredictor, self).__init__()
        self.device = torch.device('cuda') if torch.cuda.is_available() else torch.device('cpu')
        # 1 input image channel, 6 output channels, 3x3 square convolution
        # kernel
        self.D_in = D_in
        self.conv1 = nn.Conv2d(1, 6, 3)
        self.conv2 = nn.Conv2d(6, 16, 3)
        # an affine operation: y = Wx + b
        self.fc1 = nn.Linear(3744, 120)  # 6*6 from image dimension
        self.fc2 = nn.Linear(120, 84)
        self.fc_only_mask = nn.Linear(84, D_out)
#         self.fc3 = nn.Linear(88, 50) 
#         self.experimental1 = nn.Linear(88, D_out)
#         self.experimental2 = nn.Linear(D_out, D_out)
#         self.fc4 = nn.Linear(50, 20) 
#         self.fc5 = nn.Linear(20, 10)
#         self.fc6 = nn.Linear(10, D_out)

    def forward(self, masks, extra_inputs=None):
        x1 = nn.functional.max_pool2d(nn.functional.relu(self.conv1(masks)), (2, 2))
        # If the size is a square you can only specify a single number
        x1 = nn.functional.max_pool2d(nn.functional.relu(self.conv2(x1)), 2)
        x1 = x1.view(x1.size(0), -1)
        self.D_in=x1.shape
        x1 = nn.functional.relu(self.fc1(x1))
        x1 = nn.functional.relu(self.fc2(x1))
        y_pred = self.fc_only_mask(x1)
#         x2 = extra_inputs
#         x = torch.cat((x1,x2), dim=1)
#         experimental1 = nn.functional.relu(self.experimental1(x))
#         y_pred = self.experimental2(experimental1)
            
#             h_relu = nn.functional.relu(self.fc3(x))
#             h_relu = nn.functional.relu(self.fc4(h_relu))
#             h_relu = nn.functional.relu(self.fc5(h_relu))
#             y_pred = self.fc6(h_relu)
        return y_pred